#include <SPI.h>
#include <WiFi.h>
#include <WiFiUdp.h>


// Settings
const int changeDuration = 750; // Time taken for bulb colour/brightness/state transitions in ms
const int timeoutInterval = 1000; // How to wait for messages from bulb


// The LIFX Header structure
#pragma pack(push, 1)
typedef struct {
 /* frame */
 uint16_t size;
 uint16_t protocol:12;
 uint8_t  addressable:1;
 uint8_t  tagged:1;
 uint8_t  origin:2;
 uint32_t source;
 /* frame address */
 uint8_t  target[8];
 uint8_t  reserved[6];
 uint8_t  res_required:1;
 uint8_t  ack_required:1;
 uint8_t  :6;
 uint8_t  sequence;
 /* protocol header */
 uint64_t :64;
 uint16_t type;
 uint16_t :16;
 /* variable length payload follows */
} lifx_header;
#pragma pack(pop)

// Device::StatePower payload
#pragma pack(push, 1)
typedef struct {
 uint16_t level;
} lifx_payload_device_state_power;
#pragma pack(pop)

// Device::SetPower payload
#pragma pack(push, 1)
typedef struct {
 uint16_t level;
} lifx_payload_device_set_power;
#pragma pack(pop)

// Light::SetColor payload (https://lan.developer.lifx.com/docs/light-messages)
#pragma pack(push, 1)
typedef struct {
 uint8_t reserved;
 //HSBK
 uint16_t hue;
 uint16_t saturation;
 uint16_t brightness;
 uint16_t kelvin;
 
 uint32_t duration;
} lifx_payload_light_set_color;
#pragma pack(pop)

// Payload types
#define LIFX_DEVICE_GETPOWER 20
#define LIFX_DEVICE_SETPOWER 21
#define LIFX_DEVICE_STATEPOWER 22
#define LIFX_LIGHT_SETCOLOR 102

const unsigned int localPort = 8888;  // Port to listen for responses on
const unsigned int lifxPort  = 56700; // Port used to send to send to LIFX devices

// Remote IP (In this case we broadcast to the entire subnet)
IPAddress broadcast_ip(255, 255, 255, 255);
// IPAddress broadcast_ip(192, 168, 0, 49); // Bulb IP
// char targetMac[] = "d0:73:d5:36:e0:61";    // broadcast MAC

// Packet buffer size
#define LIFX_INCOMING_PACKET_BUFFER_LEN 300

// An EthernetUDP instance to let us send and receive packets over UDP
WiFiUDP Udp;



// Recieves light on/off state as boolean
bool getPower() {
	uint16_t power = 0;
	lifx_header header;

	// Initialise header structure
	memset(&header, 0, sizeof(header));

	// Setup the header
	header.size = sizeof(lifx_header); // Size of header
	header.tagged = 1;
	header.addressable = 1;
	header.protocol = 1024; // LIFX protocol number
	header.source = 123;
	header.ack_required = 1;
	header.sequence = 100;
	header.type = LIFX_DEVICE_GETPOWER;

	// Send packet
	Udp.beginPacket(broadcast_ip, lifxPort);
	Udp.write((byte*)&header, sizeof(lifx_header)); // Treat the structures like byte arrays
	Udp.endPacket();
	
	// Wait for power msg (max wait time is timeoutInterval)
	unsigned long started = millis();
	while (millis() - started < timeoutInterval) {
		int packetLen = Udp.parsePacket();
		byte packetBuffer[LIFX_INCOMING_PACKET_BUFFER_LEN];
		if (packetLen && packetLen < LIFX_INCOMING_PACKET_BUFFER_LEN) {
			Udp.read(packetBuffer, sizeof(packetBuffer));

			if (((lifx_header *)packetBuffer)->type == LIFX_DEVICE_STATEPOWER) {
				power = ((lifx_payload_device_state_power *)(packetBuffer + sizeof(lifx_header)))->level;
				return power > 0; // Light is either 0 or 65535
			// } else {
				// Serial.print("Unexpected Packet type: ");
				// Serial.println(((lifx_header *)packetBuffer)->type);
			}
		}
	}

	return false;
}

// Sets light to on or off
void setPower(bool bulbOn) {
	lifx_header header;
	lifx_payload_device_set_power payload;

	// Initialise both structures
	memset(&header, 0, sizeof(header));
	memset(&payload, 0, sizeof(payload));

	// Setup the header
	header.size = sizeof(lifx_header) + sizeof(payload); // Size of header + payload
	header.tagged = 1;
	header.addressable = 1;
	header.protocol = 1024; // LIFX protocol number
	header.source = 123;
	header.ack_required = 1;
	header.sequence = 100;
	header.type = LIFX_DEVICE_SETPOWER;

	// Setup payload
	payload.level = bulbOn ? 65535 : 0;

	// Send packet
	Udp.beginPacket(broadcast_ip, lifxPort);
	Udp.write((byte *) &header, sizeof(lifx_header)); // Treat the structures like byte arrays
	Udp.write((byte *) &payload, sizeof(payload));    // Which makes the data on wire correct
	Udp.endPacket();
}

// Function to check power state and turn light on/off accordingly
// Returns new light state
bool toggleLight() {
	const bool lightIsOn = getPower();
	
	if (lightIsOn) {
		setPower(false);  // Turn off
	} else {
		setPower(true); // Turn on
	}

  return !lightIsOn;
}

void setColor(const unsigned int hue, const unsigned int saturation, const unsigned int brightness, const unsigned int kelvin) {
   lifx_header header;
   lifx_payload_light_set_color payload;
   
   // Initialise both structures
   memset(&header, 0, sizeof(header));
   memset(&payload, 0, sizeof(payload));
   
   // Setup the header
   header.size = sizeof(lifx_header) + sizeof(payload); // Size of header + payload
   header.tagged = 1;
   header.addressable = 1;
   header.protocol = 1024; // LIFX protocol number
   header.source = 123;
   header.ack_required = 1;
   header.sequence = 100;
   header.type = LIFX_LIGHT_SETCOLOR;
   
	// HSBK
   payload.hue = hue; // 0 - 65535
   payload.saturation = saturation;
   payload.brightness = brightness;
   payload.kelvin = kelvin;
   payload.duration = changeDuration; // Milliseconds
   
   // Send packet
   Udp.beginPacket(broadcast_ip, lifxPort);
   Udp.write((byte *) &header, sizeof(lifx_header)); // Treat the structures like byte arrays
   Udp.write((byte *) &payload, sizeof(payload));    // Which makes the data on wire correct
   Udp.endPacket();
}
