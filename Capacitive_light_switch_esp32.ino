#include <CapacitiveSensor.h>
#include "heltec.h"
#include "LIFX.h"
#include "WiFi.h"

#include "secrets.h"


/*
   CapitiveSense Library Demo Sketch
   Paul Badger 2008
   Uses a high value resistor e.g. 10 megohm between send pin and receive pin
   Resistor effects sensitivity, experiment with values, 50 kilohm - 50 megohm. Larger resistor values yield larger sensor values.
   Receive pin is the sensor pin - try different amounts of foil/metal on this pin
   Best results are obtained if sensor foil and wire is covered with an insulator such as paper or plastic sheet
   https://www.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/57170/versions/2/previews/html/CapacitiveSensorExample_01.png
*/


/* SETTINGS: */
CapacitiveSensor   cs_4_2 = CapacitiveSensor(22, 18);       // 10 megohm resistor between pins x & x, pin x is sensor pin, add wire, foil
int triggerDifference = 1000; // Required touch change to trigger button press
const int screenTimeout = 60000; // How long to keep screen on (milliseconds) after startup to prevent burn-in
int greetingDuration = 3000; // How long to display hi/bye messages for
String hiMessage = "Hello!";
String byeMessage = "Goodbye!";
int calibrationIterationsRemaining = 10; // How many iterations before the button can be triggered


/* VARS: */
bool lightState = false;
bool buttonDown = false;
int lastPress;
int idleAccumulator; // Accumulator when the button is NOT pressed
int liveAccumulator; // Always updated accumulator

int getTouchReading() {
  return cs_4_2.capacitiveSensor(30);
}

void setup() {
  cs_4_2.set_CS_AutocaL_Millis(0xFFFFFFFF);     // turn off autocalibrate on channel 1 - just as an example
  
  // Initialize the Heltec ESP32 object
  Heltec.begin(true /*DisplayEnable Enable*/, true /*LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, 470E6 /**/);
  
  delay(500);
  Heltec.display -> clear();

  Heltec.display -> drawString(0, 20, "Starting...");
  Heltec.display -> display();
  
  WIFISetUp();

  // Set accumulators to the raw current sensor reading (unstable)
  idleAccumulator = getTouchReading();
  liveAccumulator = idleAccumulator;
}

void updateIdleAccumilator(int newReading) {
  idleAccumulator = (idleAccumulator * 0.9) + (newReading * 0.1);
}

void updateLiveAccumilator(int newReading) {
  liveAccumulator = (liveAccumulator * 0.7) + (newReading * 0.3);
}

void loop() {
  long start = millis();
  long touchReading = cs_4_2.capacitiveSensor(30);
  updateLiveAccumilator(touchReading);

  // Do not allow button triggering for the first n iterations (in case of parasitic capacitance during startup)
  if (calibrationIterationsRemaining > 0) {
    calibrationIterationsRemaining--;
    updateIdleAccumilator(touchReading);
    return;
  }

  const int readingChange = touchReading - idleAccumulator;
  const int accumulatorChange = liveAccumulator - idleAccumulator;
  if (accumulatorChange > triggerDifference) {
    if (!buttonDown) {
      buttonPressed();
      buttonDown = true;
    }
  } else {
    if (buttonDown) {
      buttonReleased();
      buttonDown = false;
    }
  }


  Heltec.display -> clear();
  bool displayDebug = millis() < screenTimeout;
  if (displayDebug) {
    Heltec.display -> drawString(0, 9, "Live accumulator: " + (String)liveAccumulator);
    Heltec.display -> drawString(0, 20, "Idle accumulator: " + (String)idleAccumulator);
    if (buttonDown) { Heltec.display -> drawString(0, 42, "BUTTON DOWN!"); }
  }

  // Check the button has just been pressed
  if (lastPress != 0 && millis() - lastPress < greetingDuration) {
    int greetingOffset;
    if (displayDebug) {
      greetingOffset = 31;
    } else {
      greetingOffset = 9;
      Heltec.display->setFont(ArialMT_Plain_24);
    }
    
    if (lightState) {
      Heltec.display -> drawString(0, greetingOffset, hiMessage);
    } else {
      Heltec.display -> drawString(0, greetingOffset, byeMessage);
    }
  }
  Heltec.display -> display();


  // Do not update the idle accumilator if the button is being held down (or potentially being held down)
  if (!buttonDown && readingChange < triggerDifference) {
    updateIdleAccumilator(touchReading);
  }

  delay(10); // Arbitrary delay to limit data to serial port
}

void buttonPressed() {
  lastPress = millis();
  lightState = toggleLight();
}

void buttonReleased() { }

void WIFISetUp(void)
{
  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.disconnect(true);
  delay(1000);
  WiFi.mode(WIFI_STA);
  WiFi.setAutoConnect(true);
  WiFi.begin(ssid, pass);
  delay(100);

  byte count = 0;
  while(WiFi.status() != WL_CONNECTED && count < 10)
  {
    count ++;
    delay(500);
    Heltec.display -> drawString(0, 0, "Connecting...");
    Heltec.display -> display();
  }

  Heltec.display -> clear();
  if(WiFi.status() == WL_CONNECTED)
  {
    Heltec.display -> drawString(0, 0, "Connecting...OK.");
    Heltec.display -> display();
    delay(500);
  }
  else
  {
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Connecting...Failed");
    Heltec.display -> display();
    delay(1000);
    WIFISetUp(); // Retry
  }
  Heltec.display -> drawString(0, 10, "WIFI Setup done");
  Heltec.display -> display();
  delay(500);
}
