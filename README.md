# LIFX touch-button ESP32 
A capacitive-touch sensing light switch toggle, built to work with LIFX bulbs.

Built for the Heltec 32 board. Prints the WiFi connection and calibration phase to the OLED screen along with status until a timeout after starting (to avoid burn-in for long-term standard use).

## Secrets.h
To setup a connection, ensure you create a `secrets.h` file. See `secrets EXAMPLE.h` for an example.